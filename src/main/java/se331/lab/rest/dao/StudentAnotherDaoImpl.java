package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;
import se331.lab.rest.repository.StudentRepository;

import java.util.List;

@Repository
@Slf4j
public class StudentAnotherDaoImpl implements StudentAnotherDao {
    @Autowired
    StudentRepository studentRepository;

     @Override
     public List<Student> getStudentByNameContains(String partOfName){
         return studentRepository.findByNameContains(partOfName);
     }

     @Override
     public List<Student> getStudentWhoseAdvisorNameIs(String name){
         return studentRepository.findByName(name);
     }

    @Override
    public List<Student> getAllStudent() {
//        log.info("find all student in db");
        return studentRepository.findAll();
    }
    //
    // @Override
    // public Student findById(Long id) {
    // log.info("find student from id {} from database", id);
    // return studentRepository.findById(id).orElse(null);
    //
    // }
    //
    // @Override
    // public Student saveStudent(Student student) {
    // log.info("save student to database");
    // return studentRepository.save(student);
    //
    // }
}
