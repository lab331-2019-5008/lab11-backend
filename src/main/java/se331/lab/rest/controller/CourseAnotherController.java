package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Course;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CourseAnotherService;

@Controller
@Slf4j
public class CourseAnotherController {
    @Autowired
    CourseAnotherService courseAnotherService;
    @GetMapping("/courseEnrolledMoreThan/{num}")
    public ResponseEntity<?> getCourseWhichStudentEnrolledMoreThan(@PathVariable int num) {
//        log.info("the controller is call");
//        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(this.courseAnotherService.getCourseWhichStudentEnrolledMoreThan(num)));
        return ResponseEntity.ok(courseAnotherService.getCourseWhichStudentEnrolledMoreThan(num));

    }
    @GetMapping("/courses/{courseId}")
    public ResponseEntity getCourseById(@PathVariable String courseId) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(this.courseAnotherService.getCourseById(courseId)));
    }

    @PostMapping("/courses")
    public ResponseEntity saveStudent(@RequestBody Course course) {
        return ResponseEntity.ok(courseAnotherService.saveCourse(course));
    }

}
