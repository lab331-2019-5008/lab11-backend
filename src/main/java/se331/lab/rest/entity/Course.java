package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String courseId;
    String courseName;
    String content;
    @ManyToOne
    @JsonBackReference
    Lecturer lecturer;
    @ManyToMany
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @Builder.Default
    List<Student> students = new ArrayList<>();

    public List<Student> getStudents() {
        return students;
    }
}
