package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Lecturer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String name;
    String surname;
    @OneToMany(mappedBy = "advisor")
    @Builder.Default
    List<Student> advisees = new ArrayList<>();
    @OneToMany(mappedBy = "lecturer")
    @Builder.Default
    @JsonManagedReference
    List<Course> courses = new ArrayList<>();

    public String getName() {
        return this.name;
    }

    public List<Student> getAdvisees() {
        return advisees;
    }
}
