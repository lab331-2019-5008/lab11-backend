package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import se331.lab.rest.dao.CourseAnotherDao;
import se331.lab.rest.dao.LecturerAnotherDao;
import se331.lab.rest.entity.Course;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseAnotherServiceImpl implements CourseAnotherService {
    @Autowired
    CourseAnotherDao courseAnotherDao;

    @Autowired
    LecturerAnotherDao lecturerAnotherDao;

    public CourseAnotherServiceImpl(CourseAnotherDao courseAnotherDao) {
        this.courseAnotherDao = courseAnotherDao;
    }

    @Override
    @Transactional
    public List<Course> getCourseWhichStudentEnrolledMoreThan(double num) {
        List<Course> courses = courseAnotherDao.getAllCourses();
        List<Course> output = new ArrayList<>();
        for (Course course : courses) {
            int numEnrolledStudent = course.getStudents().size();
            if (numEnrolledStudent > num) {
                output.add(course);
            }
        }
        return output;

    }

    @Transactional
    @Override
    public List<Course> getCourses() {
        return courseAnotherDao.getAllCourses();
    }

    @Override
    @Transactional
    public Course saveCourse(Course course) {
        return courseAnotherDao.saveCourse(course);
    }

    @Override
    @Transactional
    public Course getCourseById(String courseId) {
        return courseAnotherDao.getCourseById(courseId);
    }


}
