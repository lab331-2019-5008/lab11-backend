package se331.lab.rest.dao;

import se331.lab.rest.entity.Course;

import java.util.List;

public interface CourseAnotherDao {
    List<Course> getAllCourses();
    List<Course> findAll();
    Course saveCourse(Course course);
    Course getCourseById(String courseId);

    // List<Course> getCourseWhichStudentEnrolledMoreThan(int num);
}
