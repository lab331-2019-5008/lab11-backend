package se331.lab.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se331.lab.rest.dao.CourseAnotherDao;
import se331.lab.rest.dao.LecturerAnotherDao;
import se331.lab.rest.dao.StudentAnotherDao;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Student;


import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestApplicationTests {
    LecturerAnotherDao lecturerAnotherDao;
    CourseAnotherDao courseAnotherDao;
    StudentAnotherDao studentAnotherDao;

    @Test
    public void contextLoads() {
//        TestGetCourseWhichStudentEnrolledMoreThan(2.22);
//        TestGetStudentByNameContains("e");
//        TestGetStudentWhoseAdvisorNameIs("Chartchai");
    }
    @Test
    public List<Course> TestGetCourseWhichStudentEnrolledMoreThan(double num){
        List<Course> courses = courseAnotherDao.getAllCourses();
        List<Course> output = new ArrayList<>();
        for (Course course: courses) {
            int numEnrolledStudent = course.getStudents().size();
            if (numEnrolledStudent > num) {
                output.add(course);
            }
        }
        return output;
    }

    @Test
    public List<Student> TestGetStudentByNameContains(String partOfName){
//        List<Student> students = studentAnotherDao.getAllStudent();
//        List<Student> output = new ArrayList<>();
//        for (Student student: students) {
//            String studentName = student.getName();
//            if (studentName.contains(partOfName)) {
//                output.add(student);
//            }
//        }
//        return output;
        return studentAnotherDao.getStudentByNameContains(partOfName);

}


    @Test
    public List<Student> TestGetStudentWhoseAdvisorNameIs(String name){
//        List<Student> students = studentAnotherDao.getAllStudent();
//        List<Student> output = new ArrayList<>();
//        for( Student student: students) {
//            String advisor = student.getAdvisor().getName();
//            if (advisor.equals(name)) {
//                output.add(student);
//            }
//        }
//        return output;
        return studentAnotherDao.getStudentWhoseAdvisorNameIs(name);

    }

}
