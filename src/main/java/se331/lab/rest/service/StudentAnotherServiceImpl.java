package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.LecturerAnotherDao;
import se331.lab.rest.dao.StudentAnotherDao;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentAnotherServiceImpl implements StudentAnotherService {
    @Autowired
    StudentAnotherDao studentAnotherDao;
    @Autowired
    LecturerAnotherDao lecturerAnotherDao;

    @Override
    public List<Student> getStudentByNameContains(String partOfName) {
//        List<Student> students = studentAnotherDao.getAllStudent();
//        List<Student> output = new ArrayList<>();
//        for (Student student : students) {
//            String studentName = student.getName();
//            if (studentName.contains(partOfName)) {
//                output.add(student);
//            }
//        }
//
        return studentAnotherDao.getStudentByNameContains(partOfName);


    }

    @Override
    public List<Student> getStudentWhoseAdvisorNameIs(String name) {
//        List<Student> students = studentAnotherDao.getAllStudent();
//        List<Student> output = new ArrayList<>();
//        for (Student student : students) {
//            String advisor = student.getAdvisor().getName();
//            if (advisor.equals(name)) {
//                output.add(student);
//            }
//        }
//        return output;
        return studentAnotherDao.getStudentWhoseAdvisorNameIs(name);


    }
}
