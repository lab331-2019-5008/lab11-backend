package se331.lab.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Course;
import se331.lab.rest.repository.CourseRepository;

import java.util.List;

@Repository
public class CourseAnotherDaoImpl implements CourseAnotherDao {
    @Autowired
    CourseRepository courseRepository;
    // @Override
    // public List<Course> getCourseWhichStudentEnrolledMoreThan(int num){
    // return null;
    // }

    @Override
    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    @Override
    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    @Override
    public Course saveCourse(Course course) {
        return courseRepository.save(course);
    }
    @Override
    public Course getCourseById(String courseId){
        return  courseRepository.findByCourseId(courseId);
    }

}
