package se331.lab.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se331.lab.rest.controller.LecturerAnotherController;
import se331.lab.rest.controller.LecturerController;
import se331.lab.rest.controller.StudentAnotherController;
import se331.lab.rest.dto.StudentDto;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.LecturerAnotherService;
import se331.lab.rest.service.StudentAnotherService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;



@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentAnotherServiceTest {
    @Autowired
    StudentAnotherService studentAnotherService;
    @Autowired
    StudentAnotherController studentAnotherController;
    @Autowired
    LecturerAnotherService lecturerAnotherService;
    @Autowired
    LecturerAnotherController lecturerAnotherController;

    @Test
    public void testGetStudentByNameContains() {
        assertThat(MapperUtil.INSTANCE.getStudentDto(studentAnotherService.getStudentByNameContains("ra")),containsInAnyOrder(
                StudentDto.builder()
                        .id(1L)
                        .studentId("SE-001")
                        .name("Prayuth")
                        .surname("The minister")
                        .gpa(3.59)
                        .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/tu.jpg?alt=media&token=f16b5c1c-fbea-4d98-9fa3-732b69a1bae8")
                        .build(),
                StudentDto.builder()
                        .id(2L)
                        .studentId("SE-002")
                        .name("Cherprang ")
                        .surname("BNK48")
                        .gpa(4.01)
                        .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/cherprang.png?alt=media&token=2e6a41f3-3bf0-4e42-ac6f-8b7516e24d92")
                        .build()
        ));
        assertThat(studentAnotherService.getStudentByNameContains("abc"),containsInAnyOrder());

    }

    @Test
    public void testGetStudentWhoseAdvisorNameIs() {

        assertThat(MapperUtil.INSTANCE.getStudentDto(studentAnotherService.getStudentWhoseAdvisorNameIs("Chartchai")),containsInAnyOrder());
        assertThat(studentAnotherService.getStudentWhoseAdvisorNameIs("k"),containsInAnyOrder());

    }
}
